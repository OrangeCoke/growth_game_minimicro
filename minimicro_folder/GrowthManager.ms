currentNode=0
sizeIncrement=0.5


findClosestParentMoldTile = function(growthNode)
    found = false
    current = [growthNode.x,growthNode.y]
    while found!=true
        if GameMath.moveTowardsXY(current,[growthNode.parent.x,growthNode.parent.y])==false then 
            found=true 
        //if parentXY empty just return parentXY
        else 
            found = Map.getIsMold(current)
        end if
    end while
    current = [floor(current[0]),floor(current[1])]
    return current
end function

findEligableTilesInRange = function(growthNode)
    eligableTiles = []
    for i in range(growthNode.x-growthNode.size,growthNode.x+growthNode.size)
        for j in range(growthNode.y-growthNode.size,growthNode.y+growthNode.size)
            if GameMath.distance(growthNode,[i,j])<=growthNode.size and Map.getSpaceGrowable([i,j])==true then
                eligableTiles = eligableTiles + [[i,j]]
            end if
        end for
    end for
    return eligableTiles
end function

growNode = function(growthNode)
    if growthNode.size<=growthNode.maxNodeSize then 
        if Map.getIsMold(growthNode) then
            foundEmpty=false
            while foundEmpty==false
                //print "grow grown node"
                eligableTiles = findEligableTilesInRange(growthNode)
                if eligableTiles.len>0 then
                    selectedTile = round(GameMath.randRange(0,eligableTiles.len-1))
                    Map.growPointOnMap(eligableTiles[selectedTile])
                    //may save eligableTiles in mapData
                    foundEmpty=true
                end if
                if eligableTiles.len<=1 then  
                    growthNode.size = growthNode.size + sizeIncrement 
                end if
            end while
            return true
        else
            //print "grow unexpanded node"
            selectedTile = findClosestParentMoldTile(growthNode)
                found = false
            while found!=true
                GameMath.moveTowardsXY(selectedTile,[growthNode.x,growthNode.y],0.8)
                found = Map.getSpaceGrowable(selectedTile)
            end while
            //display(5).fillEllipse(floor(selectedTile[0])*8,floor(selectedTile[1])*8,3,3,color.fuchsia)
            Map.growPointOnMap(selectedTile)
            return true
        end if
    else
        if GrowthNodeManager.notFullSizeNodes.indexOf(growthNode)!=null then
            GrowthNodeManager.notFullSizeNodes.remove(GrowthNodeManager.notFullSizeNodes.indexOf(growthNode))
        end if
        return false
    end if
end function

growMold = function()
    tempGrowableNodes = GrowthNodeManager.notFullSizeNodes[0:] + GrowthNodeManager.unExpandedNodes[0:]
    grown=false
    currentNodeBeginning=currentNode
    while grown==false
        //print "nodes: " + GrowthNodeManager.growthNodes.len + " growingNode: "+ currentNode + ", x="+GrowthNodeManager.growthNodes[currentNode].x+" y="+GrowthNodeManager.growthNodes[currentNode].y
        grown = growNode(GrowthNodeManager.growthNodes[currentNode])
        outer.currentNode = currentNode+1
        if currentNode>GrowthNodeManager.growthNodes.len-1 then outer.currentNode=0
        if currentNodeBeginning == currentNode then 
            return grown //After one cycle, no growable node found
        end if
    end while
    return grown
end function