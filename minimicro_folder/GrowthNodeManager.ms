rootGrowthNode = new GrowthNode
growthNodes = [rootGrowthNode]
unExpandedNodes = growthNodes[0:]
notFullSizeNodes = []

addNode = function(node)
    if (node.expanded==false) then
        outer.unExpandedNodes=outer.unExpandedNodes + [node]
    else if node.size <= node.maxNodeSize then
        outer.notFullSizeNodes=outer.notFullSizeNodes + [node]
    end if
    outer.growthNodes = outer.growthNodes + [node]
end function

findNearestNode = function(node)
    closest = GameMath.distance(growthNodes[0], node) 
    closestNode = growthNodes[0]
    for growthNode in growthNodes
        distance = GameMath.distance(growthNode, node) 
        if distance < closest then 
            closest = distance
            closestNode = growthNode
        end if
    end for
    return closestNode
end function

findNodesinRange = function(node, maxDistance)
    foundNodes = []
    for growthNode in growthNodes
        distance = GameMath.distance(growthNode, node) 
        if distance < maxDistance then 
            foundNodes.push(growthNode)
        end if
    end for
    return foundNodes
end function

reActivateNodesinRange = function(node, maxDistance)
    for growthNode in findNodesinRange(node, maxDistance)
        if (notFullSizeNodes.indexOf(growthNode) == null) and (unExpandedNodes.indexOf(growthNode) == null)  then
            notFullSizeNodes.push(growthNode)
            growthNode.size = growthNode.size - GrowthManager.sizeIncrement
        end if
    end for
end function

getAngleVectors = function(count, startDeg, randomDeg)
    angles = []
    for i in range(0,count-1)
        angles = angles + [GameMath.getNormalizedVectorForAngle((360/count)*i+startDeg+(GameMath.randRange((-randomDeg/2),(randomDeg/2))))]
    end for
    return angles
end function

getAngleVectorsForNode = function(growthNode)
    if growthNode.parent!=0 then
        angles=getAngleVectors(growthNode.expansionCount, GameMath.getDegreeFromAtoB(growthNode.parent, growthNode) ,growthNode.expansionRandDeg)
    else
        angles=getAngleVectors(growthNode.expansionCount, GameMath.randRange(0,360) ,growthNode.expansionRandDeg)
    end if
    return angles
end function

getNormalizedVectorFromAtoB = function(a,b)
    return GameMath.getNormalizedVectorForAngle(GameMath.getDegreeFromAtoB(a, b))
end function

getRandomizedNormalizedVectorFromAtoB = function(a,b, randomDeg)
    return GameMath.getNormalizedVectorForAngle(GameMath.getDegreeFromAtoB(a, b)+(GameMath.randRange((-randomDeg/2),(randomDeg/2))))
end function

distanceToAllNodesValid = function(newGrowthNode, allowedDistance)
    for growthNode in growthNodes
        if GameMath.distance(newGrowthNode, growthNode) < allowedDistance then return false   
    end for
    return true
end function


isSplitGeneration = function(generation)
    if generation%5==0 then return true
    return false
end function

generateNode = function(newNode, parent)
    newNode.generation = parent.generation + 1
    newNode.parent = parent
    if isSplitGeneration(newNode.generation)==true then
        newNode.maxNodeSize = 3.7
        newNode.minExpansionSize = 2.7
        newNode.expansionCount = 4
        newNode.expansionRandDeg = 40
    else
        newNode.maxNodeSize = 2.2
        newNode.minExpansionSize = 1.7
        newNode.expansionCount = 1
        newNode.expansionRandDeg = 50
    end if
    parent.childNodes = parent.childNodes + [newNode]
    addNode(newNode)
    return newNode
end function

generateNodeWithClosestAsParent = function(newNode)
    generateNode(newNode, findNearestNode(newNode))
end function

expandNode = function(growthNode)
    angles=getAngleVectorsForNode(growthNode)
    for i in range(0,growthNode.expansionCount-1)
        finished=false
        count=0
        while finished==false
            newNode = new GrowthNode
            distance = 3
            currdistance=distance
            if isSplitGeneration(growthNode.generation+1) then currdistance=currdistance+2
            currdistance= currdistance*(GameMath.randRange(7,13)/10)
            newNode.x = round(growthNode.x + (angles[i][0]*currdistance))
            newNode.y = round(growthNode.y + (angles[i][1]*currdistance))
            if distanceToAllNodesValid(newNode, (currdistance/1.3))==true and Map.getNoBlockBetweenTwoPoints(newNode, growthNode) then
                generateNode(newNode, growthNode)
                finished=true
            else
                //display(5).fillEllipse(growthNode.x*8,growthNode.y*8,7,7, color.yellow)
                //display(5).fillEllipse(newNode.x*8,newNode.y*8,5,5, color.red)
                angles=getAngleVectorsForNode(growthNode)
                if count==5 then finished=true
                count=count+1
            end if
        end while
    end for 
    growthNode.expanded=true
    outer.unExpandedNodes.remove(outer.unExpandedNodes.indexOf(growthNode))
    outer.notFullSizeNodes.push(growthNode)
end function

checkExpansion = function(growthNode)
    if Map.mapData[growthNode.x][growthNode.y].type >= 1 and growthNode.size>=growthNode.minExpansionSize then
        expandNode(growthNode)
        return true
    end if
    return false
end function

expandNodes = function()
    tempunExandedNodes = unExpandedNodes[0:]
    if tempunExandedNodes.len>0 then
        for growthNode in tempunExandedNodes
            if growthNode.expanded==false then
                if checkExpansion(growthNode) == true then 
                end if
            end if
        end for
    end if
end function

drawNodes = function()
    for growthNode in growthNodes
        display(5).fillEllipse(growthNode.x*8+1,growthNode.y*8+1,6,6,color.rgba(255,255,255,150))
        display(5).fillEllipse(growthNode.x*8+2,growthNode.y*8+2,4,4,color.rgba(225, 45, 198, 200))
    end for
end function

rootGrowthNode.x = floor(Map.width/2)
rootGrowthNode.y = floor(Map.height/2)
rootGrowthNode.minExpansionSize = 0
rootGrowthNode.expansionCount = 5
rootGrowthNode.expansionRandDeg = 25//randomness of Starting node angle to each other