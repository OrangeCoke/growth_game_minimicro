import "GameMath"
import "MapCell"
import "Building"
import "Map"
import "GrowthNode"
import "GrowthNodeManager"
import "GrowthManager"
import "MapRenderer"
import "UIRenderer"
import "InputHandler"
import "SoundSystem"

load "startup.ms"
clear

mouse.visible = 0

MapRenderer.initDisplay
UIRenderer.initDisplay

MapRenderer.setupTileDisplay


InputHandler.scrollX = (Map.width*8-960)/2-60
InputHandler.scrollY = (Map.height*8-640)/2

InputHandler.syncDisplayScroll

Map.generateMapData

UIRenderer.renderButtons
UIRenderer.initMouse

display(3).clear
lasttick = time
tickTime = 0.5

GrowthNodeManager.expandNodes
MapRenderer.renderMapData

SoundSystem.playBackgroundLoop

startBonusEnergyCounter = 40
Map.startTime = time

while not key.pressed("escape")
    UIRenderer.updateMousePosition
    UIRenderer.handleTutorial
    if time - lasttick > tickTime then
        UIRenderer.updateMessageScreen
        Map.gainEnergy
        if startBonusEnergyCounter > 0 then
            Map.energy = Map.energy + 100
            startBonusEnergyCounter = startBonusEnergyCounter - 1
        end if
        while Map.energy >= 100 and Map.size < Map.maxSize
            if GrowthManager.growMold then 
                Map.energy = Map.energy - 100
                Map.size = Map.size + 1
            else
                break
            end if
            //Todo Call expandNodes after every 10 or so grow
        end while
        GrowthNodeManager.expandNodes 
        GrowthNodeManager.drawNodes
        MapRenderer.renderEnergy

        lasttick = lasttick+tickTime
    end if
    InputHandler.handleInputs
end while
Sound.stopAll
mouse.visible = 1
display(4).clear
display(2).clear
display(6).clear
display(5).clear
display(7).clear
clear