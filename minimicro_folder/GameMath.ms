
// radToDeg: convert radians to degrees
radToDeg = function(radians)
	return radians * 180 / pi
end function

// degToRad: convert degrees to radians
degToRad = function(degrees)
	return degrees * pi / 180
end function

getNormalizedVectorForAngle = function(degree)
    rad = degToRad(degree)
    return [cos(rad),sin(rad)]
end function

getDegreeFromAtoB = function(p1, p2)
    if p1 isa map then
        deltax = p2.x-p1.x
        deltay = p2.y-p1.y
    else
        deltax = p2[0]-p1[0]
        deltay = p2[1]-p1[1]
    end if
    return -(radToDeg(atan(deltax,deltay))-90)
end function

// randRange: return a uniformly distributed random number between the
// given minimum and maximum values (including min, but not including max).
randRange = function(min, max)
	return min + (max - min) * rnd
end function

// distance: Calculate the distance between two points.  Each point may be
// either a map containing "x" and "y" (such as mouse or a Sprite), or it
// can be a 2-element [x,y] list.
distance = function(p1, p2)
	if p1 isa map then
		x1 = p1.x
		y1 = p1.y
	else
		x1 = p1[0]
		y1 = p1[1]
	end if
	if p2 isa map then
		x2 = p2.x
		y2 = p2.y
	else
		x2 = p2[0]
		y2 = p2[1]
	end if	
	return sqrt((x1-x2)^2 + (y1-y2)^2)
end function

// moveTowardsXY: shift 2-element [x,y] values towards a 
// target map that contains the same, but moving no more than the given
// distance.  This is handy, for example, to make a sprite move towards
// another sprite or the mouse.  Note that this version changes the map
// passed in the first parameter, and returns `true` if any change was
// made, or `false` if the mover was already at the target.
moveTowardsXY = function(mover, target, maxDist=1)
	dx = target[0] - mover[0]
	dy = target[1] - mover[1]
	if dx == 0 and dy == 0 then return false  // already there
	dist = sqrt(dx^2 + dy^2)
	if dist < maxDist then
		mover[0] = target[0]
		mover[1] = target[1]
	else
		f = maxDist / dist
		mover[0] = mover[0] + dx * f
		mover[1] = mover[1] + dy * f
	end if
	return true   // moved, at least a little
end function

max = function(seq) 
    if seq.len == 0 then return null 
    max = seq[0] 
    for item in seq 
        if item > max then max = item 
    end for 
    return max 
end function 

min = function(seq) 
    if seq.len == 0 then return null 
    min = seq[0] 
    for item in seq 
        if item < min then min = item 
    end for 
    return min
end function 
