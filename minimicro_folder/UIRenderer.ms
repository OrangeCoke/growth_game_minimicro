buttons = []
messages = []

messageTime = time - 5
currentMessage = ""

toolTipActive = false
currentToolTip = ""

currentButton = null
currentButtonNumber = 1

pointerImage = file.loadImage("/usr/pixelart/mouse_pointer.png")
tentacleImage = file.loadImage("/usr/pixelart/tentacle_building.png")
heartImage = file.loadImage("/usr/pixelart/heart_building.png")
growthGuideImage = file.loadImage("/usr/pixelart/growth_guide.png")
deleteIconImage = file.loadImage("/usr/pixelart/delete_icon.png")
kidneyImage = file.loadImage("/usr/pixelart/kidney_building.png")
coneImage = file.loadImage("/usr/pixelart/cone_building.png")
arrowImage = file.loadImage("/usr/pixelart/arrow.png")

arrowSprite = null

firstTutorialMessageShown = false
firstTutorialDone = false

secondTutorialMessageShown = false
secondTutorialMessageDone = false

initDisplay = function()
    gfx.clear [color.clear, Map.width*8, Map.height*8]
    display(2).mode = displayMode.sprite
    self.spriteDisplay = display(2)
    display(1).mode = displayMode.pixel
    self.messageDisplay = display(1)
    self.messageDisplay.clear [color.clear,self.messageDisplay.width,self.messageDisplay.height]
    self.messageDisplay.color = color.black
    self.messageDisplay.fillRect(700,556,240,52)
end function

initMouse = function()
    self.mouseSprite = new Sprite
    self.mouseSprite.image = file.loadImage("/usr/pixelart/mouse_pointer.png")
    self.spriteDisplay.sprites.push(self.mouseSprite)
    self.updateMousePosition
end function

printMessage = function(message, duration=3)
    self.currentMessage = message
    self.messageDisplay.color = color.black
    self.messageDisplay.fillRect(120,420,self.messageDisplay.width-120,32)
    self.messageDisplay.print(self.currentMessage,124,424,color.yellow)
    self.messageTime = time+duration
end function

changeToolTip = function(toolTip)
    self.currentToolTip = toolTip
end function

checkToolTip = function()
    anyTip=false
    for button in buttons
        if button.contains(mouse) then
            self.changeToolTip(button.toolTip)
            anyTip=true
        end if
    end for
    if self.toolTipActive == true then
       anyTip=true 
    end if
    if anyTip==false then self.changeToolTip("")
    self.toolTipActive = false
end function

updateMessageScreen = function()
    fadeOutPercent = time - self.messageTime
    if self.currentToolTip=="" then
        self.messageDisplay.color = color.clear
        self.messageDisplay.fillRect(120,12,self.messageDisplay.width,32)
    else
        self.messageDisplay.color = color.black
        self.messageDisplay.fillRect(120,12,self.messageDisplay.width,32)
        self.messageDisplay.print(self.currentToolTip,124,16,color.yellow)
    end if
    if fadeOutPercent >= 0 and fadeOutPercent <= 1 then
        self.messageDisplay.color = color.lerp(color.black,color.clear,fadeOutPercent)
        self.messageDisplay.fillRect(120,420,self.messageDisplay.width-120,32)
        self.messageDisplay.print(self.currentMessage,124,424,color.lerp(color.yellow,color.clear))
    else if fadeOutPercent > 1 then
        self.messageDisplay.color = color.clear
        self.messageDisplay.fillRect(120,420,self.messageDisplay.width-120,32)
    end if
end function

updateMousePosition = function()
    if mouse.x >= 120 then
        mouseOverTileX = floor((mouse.x+InputHandler.scrollX)/8)
        mouseOverTileY = floor((mouse.y+InputHandler.scrollY)/8)
        if currentButton == buttons[0] then
            self.mouseSprite.image = self.tentacleImage
            self.renderBuildingPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.colorMousePointer
            if self.mouseSprite.tint == color.lime then
                self.toolTipActive = true
                self.changeToolTip("Predicted energy gain: " + round(Map.energyGainFromMap(mouseOverTileX,mouseOverTileY),2) + "/tick") 
            end if
        else if currentButton == buttons[1] then
            self.mouseSprite.image = self.heartImage
            self.renderBuildingPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.colorMousePointer
        else if currentButton == buttons[4] then
            self.mouseSprite.image = self.kidneyImage
            self.renderBuildingPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.colorMousePointer
        else if currentButton == buttons[5] then
            self.mouseSprite.image = self.coneImage
            self.renderBuildingPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.colorMousePointer
        else if currentButton == buttons[2] then
            self.mouseSprite.image = self.growthGuideImage
            self.renderOtherPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.mouseSprite.tint = color.white
        else if currentButton == buttons[3] then
            self.mouseSprite.image = self.deleteIconImage
            self.renderOtherPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.mouseSprite.tint = color.white
        else
            self.mouseSprite.image = self.pointerImage
            self.renderOtherPointerSpritePosition(mouseOverTileX,mouseOverTileY)
            self.mouseSprite.tint = color.yellow
        end if
    else
        self.mouseSprite.image = pointerImage
        self.mouseSprite.tint = color.yellow
        self.mouseSprite.x = mouse.x
        self.mouseSprite.y = mouse.y
    end if
end function

renderBuildingPointerSpritePosition = function(mouseOverTileX,mouseOverTileY)
    self.mouseSprite.x = (mouseOverTileX)*8 + mouseSprite.image.width/2 - InputHandler.scrollX
    self.mouseSprite.y = (mouseOverTileY)*8 + mouseSprite.image.height/2 - InputHandler.scrollY
end function

renderOtherPointerSpritePosition = function(mouseOverTileX,mouseOverTileY)
    self.mouseSprite.x = (mouseOverTileX)*8 + mouseSprite.image.width/4 - InputHandler.scrollX
    self.mouseSprite.y = (mouseOverTileY)*8 + mouseSprite.image.width/4 - InputHandler.scrollY
end function

colorMousePointer = function()
    mouseOverTileX = floor((mouse.x+InputHandler.scrollX)/8)
    mouseOverTileY = floor((mouse.y+InputHandler.scrollY)/8)
    if Map.canPlaceBuilding(mouseOverTileX,mouseOverTileY,self.currentButtonNumber) then
        self.mouseSprite.tint = color.lime
    else
        self.mouseSprite.tint = color.red
    end if
end function

addSprite = function(x,y,imagePath)
    sprite = new Sprite
    sprite.image = file.loadImage(imagePath)
    sprite.x = x
    sprite.y = y    
    self.spriteDisplay.sprites.push sprite
    return sprite
end function

addLocalBoundsToSprite = function(sprite)
    sprite.localBounds = new Bounds
    sprite.localBounds.width = sprite.image.width
    sprite.localBounds.height = sprite.image.height
end function

addButton = function(x,y,imagePath,toolTip="")
    buttonBase = self.addSprite(x,y,"/usr/pixelart/button_base.png")
    buttonBase.toolTip = toolTip
    self.addLocalBoundsToSprite(buttonBase)
    self.addSprite(x,y,imagePath)
    buttons.push buttonBase
end function

renderButtons = function()
    self.addSprite(44,320,"/usr/pixelart/ui_background.png")
    self.addButton(64,576,"/usr/pixelart/tentacle_building.png","Generates energy [Cost: 1000 energy]")
    self.addButton(64,496,"/usr/pixelart/heart_building.png", "Increases max size for 100 [Cost: 3000 energy]")
    self.addButton(64,96,"/usr/pixelart/growth_guide.png", "Places guide to grow to on map (Only 1 at a time)")
    self.addButton(64,176,"/usr/pixelart/delete_icon.png", "Removes energy and size organs")
    self.addButton(64,416,"/usr/pixelart/kidney_building.png", "Removes obstructions around organ [Cost: 3000 energy]")
    self.addButton(64,336,"/usr/pixelart/cone_building.png", "The ultimate goal [Cost: 10000 energy, min size 1500]")
end function

selectButton = function()
    count = 2
    for button in buttons
        if button.contains(mouse) then
            SoundSystem.playClick
            if self.currentButton != null then
                self.currentButton.tint = color.white
            end if
            self.currentButton = button
            self.currentButton.tint = color.blue
            self.currentButtonNumber = count
        end if
    count = count + 1
    end for
end function

showFirstTutorialMessage = function()
    self.arrowSprite = new Sprite
    self.arrowSprite.image = arrowImage
    self.arrowSprite.x = 136
    self.arrowSprite.y = buttons[0].y

    self.spriteDisplay.sprites.push self.arrowSprite
    self.printMessage("Select Tentacle and click on Pink Organism to place.",8)
    self.firstTutorialMessageShown = true
end function
showSecondTutorialMessage = function()
    self.arrowSprite.image = arrowImage
    self.arrowSprite.x = 136
    self.arrowSprite.y = buttons[1].y

    self.printMessage("Place cardio chamber to increase max size.",8)
    self.secondTutorialMessageShown = true
end function


clearTutorial = function()
    self.arrowSprite.image = null
end function

handleTutorial = function()
    if not firstTutorialMessageShown then
        UIRenderer.showFirstTutorialMessage
    else if not secondTutorialMessageShown and Map.size >= 125 then
        self.showSecondTutorialMessage
    end if
    if self.currentButton == self.buttons[0] and self.firstTutorialDone == false and self.firstTutorialMessageShown == true then
        self.firstTutorialDone = true
        UIRenderer.clearTutorial
    else if self.currentButton == self.buttons[1] and self.secondTutorialMessageDone == false and self.secondTutorialMessageShown == true then
        self.secondTutorialMessageDone = true
        UIRenderer.clearTutorial
        self.printMessage("Get to size 1500 and build a spore launcher to win.",8)
    end if
end function