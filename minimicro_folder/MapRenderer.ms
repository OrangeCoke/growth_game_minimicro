
inited = false

setupTileDisplay = function()
    //Set tiles to be 8x8 pixel
    self.tileDisplay.tileSet = file.loadImage("/usr/pixelart/organism_tileset.png")
    self.tileDisplay.cellSize = 8
    self.tileDisplay.tileSetTileSize = 8
    self.tileDisplay.extent = [Map.width,Map.height]
end function

renderMapData = function()
    for x in range(0,Map.width-1)
        for y in range(0,Map.height-1)
            if Map.mapData[x][y].type >= 1 then
                if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x, y, 7
                else if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) then
                    self.tileDisplay.setCell x,y, 17
                else if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 1
                else if Map.getIsMold([x+1,y]) and Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 8
                else if Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 10
                else if Map.getIsMold([x-1,y]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 2
                else if Map.getIsMold([x+1,y]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 0
                else if Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) then
                    self.tileDisplay.setCell x,y, 18
                else if Map.getIsMold([x+1,y]) and Map.getIsMold([x,y+1]) then
                    self.tileDisplay.setCell x,y, 16
                else if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) then
                    self.tileDisplay.setCell x,y, 5
                else if Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 11
                else if Map.getIsMold([x+1,y]) then
                    self.tileDisplay.setCell x,y, 4
                else if Map.getIsMold([x-1,y]) then
                    self.tileDisplay.setCell x,y, 6
                else if Map.getIsMold([x,y+1]) then
                    self.tileDisplay.setCell x,y, 19
                else if Map.getIsMold([x,y-1]) then
                    self.tileDisplay.setCell x,y, 3
                else
                    self.tileDisplay.setCell x,y, 12
                end if
            else if Map.mapData[x][y].type == -1 then
                self.tileDisplay.setCell x,y, 13
            else if Map.mapData[x][y].type == 0 then
                self.tileDisplay.setCell x,y, null
            end if
            //age tinting makes game run very poorly
            //self.tileDisplay.setCellTint(x, y, color.lerp(color.white,color.gray,(now-Map.mapData[x][y].age)/600))
        end for
    end for
end function

renderMapDataForPoint = function(point)
    if self.inited then 
        for x in range(GameMath.max([0,point[0]-2]),GameMath.min([point[0]+2,Map.width-1]))
            for y in range(GameMath.max([0,point[1]-2]),GameMath.min([point[1]+2,Map.height-1]))
                if Map.mapData[x][y].type >= 1 then
                    if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x, y, 7
                    else if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) then
                        self.tileDisplay.setCell x,y, 17
                    else if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 1
                    else if Map.getIsMold([x+1,y]) and Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 8
                    else if Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 10
                    else if Map.getIsMold([x-1,y]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 2
                    else if Map.getIsMold([x+1,y]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 0
                    else if Map.getIsMold([x-1,y]) and Map.getIsMold([x,y+1]) then
                        self.tileDisplay.setCell x,y, 18
                    else if Map.getIsMold([x+1,y]) and Map.getIsMold([x,y+1]) then
                        self.tileDisplay.setCell x,y, 16
                    else if Map.getIsMold([x+1,y]) and Map.getIsMold([x-1,y]) then
                        self.tileDisplay.setCell x,y, 5
                    else if Map.getIsMold([x,y+1]) and Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 11
                    else if Map.getIsMold([x+1,y]) then
                        self.tileDisplay.setCell x,y, 4
                    else if Map.getIsMold([x-1,y]) then
                        self.tileDisplay.setCell x,y, 6
                    else if Map.getIsMold([x,y+1]) then
                        self.tileDisplay.setCell x,y, 19
                    else if Map.getIsMold([x,y-1]) then
                        self.tileDisplay.setCell x,y, 3
                    else
                        self.tileDisplay.setCell x,y, 12
                    end if
                else if Map.mapData[x][y].type == -1 then
                    if self.tileDisplay.cell(x,y) != 13 then
                        self.tileDisplay.setCell x,y, 13
                    end if
                else if Map.mapData[x][y].type == 0 then
                    if self.tileDisplay.cell(x,y) != null then
                        self.tileDisplay.setCell x,y, null
                    end if
                end if
            end for
        end for
    end if
end function

renderEnergy = function()
    self.textDisplay.clear
    self.textDisplay.color = color.yellow
    self.textDisplay.column = 50
    self.textDisplay.row = 24
    self.textDisplay.print "Energy: " + floor(Map.energy)
    self.textDisplay.color = color.lime
    self.textDisplay.column = 50
    self.textDisplay.row = 23
    self.textDisplay.print "Size: " + Map.size + "/" + Map.maxSize
end function

renderBuilding = function(building)
    foundBuildingType = true
    if building.type == 2 then
        buildingImage = file.loadImage("/usr/pixelart/tentacle_building.png")
    else if building.type == 3 then
        buildingImage = file.loadImage("/usr/pixelart/heart_building.png")
    else if building.type == 6 then
        buildingImage = file.loadImage("/usr/pixelart/kidney_building.png")
    else if building.type == 7 then
        buildingImage = file.loadImage("/usr/pixelart/cone_building.png")
    else
        foundBuildingType = false
    end if
    if foundBuildingType then
        buildingSprite = new Sprite
        buildingSprite.image = buildingImage
        buildingSprite.x = (building.x)*8 + buildingImage.width/2
        buildingSprite.y = (building.y)*8 + buildingImage.height/2
        buildingSprite.localBounds = new Bounds
        buildingSprite.localBounds.width = buildingSprite.image.width
        buildingSprite.localBounds.height = buildingSprite.image.height
        tintColor = color.rgba(255,255,255,124)
        buildingSprite.tint = tintColor
        building.sprite = buildingSprite
        self.spriteDisplay.sprites.push buildingSprite
    end if
end function

renderGrowthGuide = function()
    if self.growthGuideSprite == null then
        self.growthGuideSprite = new Sprite
        self.growthGuideSprite.image = file.loadImage("/usr/pixelart/growth_guide.png")
        self.spriteDisplay.sprites.push self.growthGuideSprite
    end if
    self.growthGuideSprite.x = Map.growth_guide.x*8+4
    self.growthGuideSprite.y = Map.growth_guide.y*8+4
end function

renderEnergyDistribution = function()
    for x in range(0,Map.width-1)
        for y in range(0,Map.height-1)
            renderEnergyDistributionPoint(x,y)
        end for
    end for
end function

renderEnergyDistributionPoint = function(x,y)
    pixelDisplay.color = color.rgb(Map.mapData[x][y].energy * 255,(255 - Map.mapData[x][y].energy*255)/16,(255 - Map.mapData[x][y].energy*255)/8)
    pixelDisplay.fillRect x*8, y*8, 8, 8
end function

initDisplay = function()
    display(6).mode = displayMode.tile
    self.tileDisplay = display(6)
    display(4).mode = displayMode.sprite
    self.spriteDisplay = display(4)
    display(7).mode = displayMode.pixel
    self.pixelDisplay = display(7)
    self.pixelDisplay.clear(color.clear,Map.width*8,Map.height*8)
    display(0).mode = displayMode.text
    display(0).color = color.yellow
    self.textDisplay = display(0)
    self.growthGuideSprite = null
    self.inited = true
end function