background_loop_track = file.loadSound("/usr/sound/tentacle_jam_loop_portion.wav")
click_sound = file.loadSound("/usr/sound/click.wav")

playBackgroundLoop = function()
    background_loop_track.loop = 1
    background_loop_track.play 0.2
end function

playClick = function()
    click_sound.play
end function