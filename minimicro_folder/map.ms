width = 126
height = 126

buildings = []
mapData = []

energy = 0
size = 1
maxSize = 150
sizeIncrement = 100

growth_guide = null

tentacleBuilding = 2
heartBuilding = 3
kidneyBuilding = 6
coneBuilding = 7

startTime = 0

getIsMold = function(point)
    if point isa map then
        if point.x>=0 and point.x<Map.width and point.y>=0 and point.y<Map.height then
        if Map.mapData[floor(point.x)][floor(point.y)].type>=1 then return true
        end if
        return false
    else
        if point[0]>=0 and point[0]<Map.width and point[1]>=0 and point[1]<Map.height then
        if Map.mapData[floor(point[0])][floor(point[1])].type>=1 then return true
        end if
        return false
    end if
end function

getSpaceGrowable = function(point)
    if point isa map then
        if Map.mapData[floor(point.x)][floor(point.y)].type == 0 then return true
        return false
    else
        if Map.mapData[floor(point[0])][floor(point[1])].type == 0 then return true
        return false
    end if
end function

getSpaceBlocked = function(point)
    if point isa map then
        if Map.mapData[floor(point.x)][floor(point.y)].type == -1 then return true
        return false
    else
        if Map.mapData[floor(point[0])][floor(point[1])].type == -1 then return true
        return false
    end if
end function



getNoBlockBetweenTwoPoints = function (point1, point2)
    if point1 isa map then
        current = [point1.x,point1.y]
    else
        current = [point1[0],point1[1]]
    end if
    if point2 isa map then
        destination = [point2.x,point2.y]
    else
        destination = point2
    end if
    finished = false
    foundBlocked = false
    while foundBlocked!=true and finished!=true
        if GameMath.moveTowardsXY(destination,current)==false then 
            foundBlocked = Map.getSpaceBlocked(destination)
            finished = true
        else 
            foundBlocked = Map.getSpaceBlocked(destination)
        end if
    end while
    return not foundBlocked
end function

getSpaceFree = function(x,y,size)
    for i in range(0,size-1)
        for j in range(0,size-1)
            if mapData[x+i][y+j].type != 1 then
                return false
            end if
        end for
    end for
    return true
end function

changeTypeOfPoint = function(point,type)
    if point isa map then
        Map.mapData[point.x][point.y].changeType(type)
        MapRenderer.renderMapDataForPoint([point.x,point.y])
	else
        Map.mapData[point[0]][point[1]].changeType(type)
        MapRenderer.renderMapDataForPoint(point)
	end if
end function

growPointOnMap = function(point)
    if point isa map then
        //print "        grow tile "+ [floor(point.x),floor(point.y)]+" was mold="+getIsMold(point)
        //display(5).fillEllipse(floor(point.x)*8,floor(point.y)*8,5,5,color.red)
        changeTypeOfPoint([floor(point.x),floor(point.y)],1)
	else
        //print "        grow tile "+ [floor(point[0]),floor(point[1])]+" was mold="+getIsMold(point)
        //display(5).fillEllipse(floor(point[0])*8,floor(point[1])*8,5,5,color.red)
        changeTypeOfPoint([floor(point[0]),floor(point[1])],1)
	end if
end function

changeTypeOfArea = function(x,y,size,type)
    for i in range(0,size-1)
        for j in range(0,size-1)
            if x+i < self.width and y+j < self.height then changeTypeOfPoint([x+i,y+j],type)
        end for
    end for
end function

canPlaceBuilding = function(x,y,buildingType)
    if self.energy < 0 then
        return 0
    end if
    if x >= self.width or x < 0 or y >= self.height or y < 0 then //Check if values are out of bounds
        return 0
    end if
    if buildingType == 2 then
        return self.getSpaceFree(x,y,2)
    else if buildingType == 3 then
        return self.getSpaceFree(x,y,4)
    else if buildingType == 6 then
        return self.getSpaceFree(x,y,3)
    else if buildingType == 7 then
        return self.getSpaceFree(x,y,4) and self.size >= 1500
    end if
    return 0
end function

randomPointOnMap = function()
    x = floor(rnd*self.width)
    y = floor(rnd*self.height)
    return {"x": x,"y": y}
end function

placeBuilding = function(x,y)
    buildingType = 1
    if UIRenderer.currentButton == UIRenderer.buttons[0] then
        buildingType = 2
    else if UIRenderer.currentButton == UIRenderer.buttons[1] then
        buildingType = 3
    else if UIRenderer.currentButton == UIRenderer.buttons[4] then
        buildingType = 6
    else if UIRenderer.currentButton == UIRenderer.buttons[5] then
        buildingType = 7
    end if

    if self.canPlaceBuilding(x,y,buildingType) == 1 then
        //Add building to buildings
        building = new Building
        building.x = x
        building.y = y
        building.type = buildingType
        building.active = 0
        self.buildings.push(building)
        if buildingType == 2 then 
            self.changeTypeOfArea(x,y,2,2)
            self.energy = self.energy - 1000
        else if buildingType == 3 then
            self.changeTypeOfArea(x,y,4,3)
            self.energy  = self.energy - 3000
        else if buildingType == 6 then
            self.changeTypeOfArea(x,y,3,4)
            self.energy = self.energy - 3000
        else if buildingType == 7 then
            self.changeTypeOfArea(x,y,4,5)
            self.energy = self.energy - 10000
        end if
        MapRenderer.renderBuilding(building)
    end if
end function

generateMapData = function()
    
    for x in range(0,width-1)
        mapData.push([])
        for y in range(0,height-1)
            cell = new MapCell
            mapData[x].push(cell)
        end for
    end for
    
    self.changeTypeOfArea(width/2,height/2,1,1)
    energyPoints = []

    numberOfPoints = 5

    for i in range(1,numberOfPoints)
        energyPoints.push self.randomPointOnMap
    end for
    
    for point in energyPoints
        mapData[point.x][point.y].setEnergy(rnd*0.5 + 0.5)
    end for

    for x in range(0, width-1)
        for y in range (0, height-1)
            closestPoint = null
            closestDistance = 0
            for point in energyPoints
                distance = GameMath.distance([x,y],point)
                if closestPoint == null or distance < closestDistance then
                    closestPoint = point
                    closestDistance = distance
                end if
            end for
            if not (closestPoint.x == x and closestPoint.y == y) then
                mapData[x][y].setEnergy(mapData[closestPoint.x][closestPoint.y].energy/closestDistance)
            end if
            MapRenderer.renderEnergyDistributionPoint(x,y)
        end for
    end for

    for x in range(0, width-1,3)
        for y in range(0, height-1,3)
            if mapData[x][y].type == 0 then
                distanceToCenter = GameMath.distance([x,y],[self.width/2,self.height/2])
                if distanceToCenter >= 10 then
                    distanceModifier = distanceToCenter / (GameMath.min([self.width, self.height])/2)
                    randomNumber = GameMath.randRange(0,1) + distanceModifier*distanceModifier/1.5
                    if randomNumber > 1 then
                        self.changeTypeOfArea(x,y,3,-1)
                    end if
                end if
            end if
        end for
    end for
end function

energyGainFromMap = function(x,y)
    energyGain = 0
    for i in range(-1,2)
        for j in range(-1,2)
            energyGain = energyGain + mapData[x+i][y+j].energy + 0.25
        end for
    end for
    return energyGain
end function

gainEnergy = function()
    for building in self.buildings
        if building.type == tentacleBuilding and building.active == 1 then
            self.energy = self.energy + energyGainFromMap(building.x,building.y)
        end if
        if self.energy >= 0 then
            if building.active == 0 then
                building.active = 1
                building.sprite.tint = color.white
                if building.type == heartBuilding then
                    self.maxSize = self.maxSize + self.sizeIncrement
                else if building.type == kidneyBuilding then
                    self.purifyArea(building.x+1,building.y+1)
                else if building.type == coneBuilding then
                    timeTaken =  floor(time - self.startTime)
                    minutes = floor(timeTaken/60)
                    seconds = timeTaken % 60
                    UIRenderer.printMessage("!YOU WON! It took " + minutes + "m" + seconds + "s" + " seconds to launch a new colony.")
                end if
            end if
        end if
    end for
    self.energy = self.energy + 15
end function

deleteBuilding = function(building)
    if building != null and building.type != 6 then
        MapRenderer.spriteDisplay.sprites.remove(MapRenderer.spriteDisplay.sprites.indexOf(building.sprite))
    
        if building.type == tentacleBuilding then
            self.changeTypeOfArea(building.x,building.y,2,1)
        else if building.type == heartBuilding then
            self.changeTypeOfArea(building.x,building.y,4,1)          
            self.maxSize = self.maxSize - self.sizeIncrement
        end if
            self.buildings.remove(self.buildings.indexOf(building))
    end if
end function

purifyArea = function(x,y)
    purifyRange = 20
    for i in range(0,self.width-1)
        for j in range(0, self.height-1)
            if mapData[i][j].type == -1 and GameMath.distance([i,j],[x,y]) < purifyRange then
                changeTypeOfPoint([i,j],0)
            end if
        end for
    end for
    GrowthNodeManager.reActivateNodesinRange([x,y],purifyRange)
end function