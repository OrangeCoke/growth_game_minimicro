justClicked = 0
lastKeyInput = 0
scrollY = 0
scrollX = 0

handleInputs = function()
    if mouse.button then
        if self.justClicked < 1 then
            if mouse.x >= 120 then
                if UIRenderer.currentButton == UIRenderer.buttons[0] or UIRenderer.currentButton == UIRenderer.buttons[1] or UIRenderer.currentButton == UIRenderer.buttons[4] or UIRenderer.currentButton == UIRenderer.buttons[5] then
                    Map.placeBuilding(floor((mouse.x+self.scrollX)/8),floor((mouse.y+self.scrollY)/8))
                else if UIRenderer.currentButton == UIRenderer.buttons[2] then
                    if Map.growth_guide != null then
                        if Map.getIsMold(Map.growth_guide) == false then
                            UIRenderer.printMessage("Can only place growth node when organism reached old one.")
                            self.justClicked = self.justClicked+1
                            return
                        end if
                    end if
                    Map.growth_guide = {"x": floor((mouse.x+self.scrollX)/8),"y": floor((mouse.y+self.scrollY)/8)}
                    if Map.getSpaceBlocked(Map.growth_guide) then 
                        Map.growth_guide = null
                        UIRenderer.printMessage("Can not be placed on obstructions.")
                        return
                    end if
                    newNode = new GrowthNode
                    newNode.x = Map.growth_guide.x
                    newNode.y = Map.growth_guide.y
                    nearestNode = GrowthNodeManager.findNearestNode(newNode)
                    if GameMath.distance(newNode, nearestNode) > 5 then
                        Map.growth_guide = null
                        UIRenderer.printMessage("Can not be placed so far from other Nodes.")
                        return
                    end if
                    if GameMath.distance(newNode, nearestNode) < 1 then
                        Map.growth_guide = null
                        UIRenderer.printMessage("Can not be placed so close to other Nodes.")
                        return
                    end if
                    GrowthNodeManager.generateNode(newNode, nearestNode)
                    MapRenderer.renderGrowthGuide

                else if UIRenderer.currentButton == UIRenderer.buttons[3] then
                    for building in Map.buildings
                        if building.sprite.contains([mouse.x + self.scrollX, mouse.y + self.scrollY]) then
                            Map.deleteBuilding(building)
                        end if
                    end for
                end if
            else
               UIRenderer.selectButton
            end if
        end if
        self.justClicked = self.justClicked + 1
    else
        self.justClicked = 0
    end if

    UIRenderer.checkToolTip()
    
    if mouse.button(1) then
       if UIRenderer.currentButton != null then
            SoundSystem.playClick
            UIRenderer.currentButton.tint = color.white
            UIRenderer.currentButton = null
        end if
    end if

    if key.pressed("h") then
        //remove this before shipping
        UIRenderer.spriteDisplay.mode = displayMode.off
    end if

    timeChecker = time - self.lastKeyInput

    if key.pressed("up") or key.pressed("w") then
        if timeChecker >= 0.015 then
            self.scrollY = self.scrollY + 1
            MapRenderer.tileDisplay.scrollY = self.scrollY
            MapRenderer.pixelDisplay.scrollY = self.scrollY
            MapRenderer.spriteDisplay.scrollY = self.scrollY
            gfx.scrollY = gfx.scrollY + 1
            self.lastKeyInput = time
        end if
    end if

    if key.pressed("down") or key.pressed("s") then
        if timeChecker >= 0.015 then
            self.scrollY = self.scrollY - 1
            MapRenderer.tileDisplay.scrollY = self.scrollY
            MapRenderer.pixelDisplay.scrollY = self.scrollY
            MapRenderer.spriteDisplay.scrollY = self.scrollY
            gfx.scrollY = gfx.scrollY - 1
            self.lastKeyInput = time
        end if
    end if

    if key.pressed("right") or key.pressed("d") then
        if timeChecker >= 0.015 then
            self.scrollX = self.scrollX + 1
            MapRenderer.tileDisplay.scrollX = self.scrollX
            MapRenderer.pixelDisplay.scrollX = self.scrollX
            MapRenderer.spriteDisplay.scrollX = self.scrollX
            gfx.scrollX = gfx.scrollX + 1
            self.lastKeyInput = time
        end if
    end if

    if key.pressed("left") or key.pressed("a") then
        if timeChecker >= 0.015 then
            self.scrollX = self.scrollX - 1
            MapRenderer.tileDisplay.scrollX = self.scrollX
            MapRenderer.pixelDisplay.scrollX = self.scrollX
            MapRenderer.spriteDisplay.scrollX = self.scrollX
            gfx.scrollX = gfx.scrollX - 1
            self.lastKeyInput = time
        end if
    end if

    if key.pressed("m") then
        Sound.stopAll
    end if

end function

syncDisplayScroll = function()
    MapRenderer.tileDisplay.scrollX = self.scrollX
    MapRenderer.pixelDisplay.scrollX = self.scrollX
    MapRenderer.spriteDisplay.scrollX = self.scrollX
    gfx.scrollX = self.scrollX
    MapRenderer.tileDisplay.scrollY = self.scrollY
    MapRenderer.pixelDisplay.scrollY = self.scrollY
    MapRenderer.spriteDisplay.scrollY = self.scrollY
    gfx.scrollY = self.scrollY
end function